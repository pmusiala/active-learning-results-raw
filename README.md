## This repository conatins accuracy results for different active learning strategies.

Naming convention for results:

\<Stream generator\>.\<Active learning strategy\>.\<Classifier\>.\<Budget\>.\<Concept drift size\>.\<Chunk size\>

example:
SEAGenerator.VarUncertainty.NaiveBayes.0.3.0.0.None

* Generator: SEAGenerator
* Strategy: VarUncertainty
* Classifier: NaiveBayes
* Budget: 0.3
* Drift size: 0.0
* Chunk size: None 