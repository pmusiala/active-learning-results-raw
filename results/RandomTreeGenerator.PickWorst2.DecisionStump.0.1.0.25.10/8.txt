learning evaluation instances,evaluation time (cpu seconds),model cost (RAM-Hours),classified instances,classifications correct (percent),Kappa Statistic (percent),Kappa Temporal Statistic (percent),Kappa M Statistic (percent),model training instances,model serialized size (bytes),labeling cost,newThreshold,maxPosterior,accuracyBaseLearner (percent),Change detected,Warning detected
100000.0,0.15625,1.1402234021160337E-9,100000.0,61.5,19.511297686942843,18.25902335456475,-3.7735849056603805,9090.0,9208.0,0.0,1.0,0.0,?,0.0,12.0
200000.0,0.328125,2.3919791500601505E-9,200000.0,62.6,21.500803882142066,23.203285420944557,1.8372703412073508,18181.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
300000.0,0.484375,3.5199142682055635E-9,300000.0,52.7,7.37622094981475,-5.111111111111116,-26.809651474530828,27272.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
400000.0,0.640625,4.645585755093231E-9,400000.0,60.099999999999994,14.58955894818861,11.33333333333332,-10.22099447513813,36363.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
500000.0,0.796875,5.765113099995587E-9,500000.0,61.0,17.40612888879476,18.580375782880996,-6.849315068493158,45454.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
600000.0,0.953125,6.83548730901546E-9,600000.0,55.7,13.878239772311973,11.222444889779569,-13.299232736572872,52358.0,9112.0,0.0,1.0,0.0,?,1.0,55.0
700000.0,1.125,8.053450276040368E-9,700000.0,58.199999999999996,10.924720734082573,6.4876957494406975,-12.064343163538885,61449.0,9208.0,0.0,1.0,0.0,?,0.0,37.0
800000.0,1.28125,9.140640031546354E-9,800000.0,54.50000000000001,11.288750243712228,4.411764705882358,-22.641509433962252,70540.0,9112.0,0.0,1.0,0.0,?,0.0,19.0
900000.0,1.4375,1.0224596028112703E-8,900000.0,60.099999999999994,17.241207656121656,16.176470588235286,-2.0460358056266004,79631.0,9208.0,0.0,1.0,0.0,?,0.0,8.0
1000000.0,1.59375,1.1298527371966176E-8,1000000.0,53.1,8.25006064494925,1.2631578947368434,-21.50259067357512,88722.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
1100000.0,1.75,1.2370195084561903E-8,1100000.0,59.8,15.533442033248726,17.113402061855663,-1.0050251256281415,97813.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
1200000.0,1.90625,1.3499423706283171E-8,1200000.0,55.400000000000006,12.10506400958568,1.97802197802198,-16.145833333333318,106903.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
1300000.0,2.0625,1.4626388696746693E-8,1300000.0,63.1,20.56042574445,19.078947368421048,-6.340057636887614,115994.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
1400000.0,2.21875,1.5747209545224903E-8,1400000.0,60.6,16.80743243243243,15.086206896551715,-7.065217391304354,125085.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
1500000.0,2.359375,1.6746926121413707E-8,1500000.0,54.900000000000006,12.041876647995887,6.625258799171848,-15.051020408163248,134176.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
1600000.0,2.515625,1.785157817519373E-8,1600000.0,56.39999999999999,14.718826405867958,5.217391304347807,-16.266666666666683,143267.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
1700000.0,2.65625,1.8843727755463784E-8,1700000.0,64.8,25.679128081043533,21.95121951219512,7.124010554089716,152358.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
1800000.0,2.78125,1.972072317989336E-8,1800000.0,65.0,24.702359182654867,22.394678492239464,1.960784313725492,161449.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
1900000.0,2.9375,2.0810823318445018E-8,1900000.0,60.8,17.72276770534676,15.517241379310336,-2.887139107611551,170540.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
2000000.0,3.09375,2.1894779315011365E-8,2000000.0,60.699999999999996,18.57082473457819,18.295218295218287,1.5037593984962419,179631.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
2100000.0,3.234375,2.28648099841343E-8,2100000.0,61.5,19.0986114379854,11.494252873563227,-6.060606060606066,188722.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
2200000.0,3.375,2.3825818465815647E-8,2200000.0,57.3,16.696580462909484,9.72515856236785,-19.60784313725492,197813.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
2300000.0,3.53125,2.4958927598264478E-8,2300000.0,63.3,22.316204797344774,21.244635193133043,5.167958656330754,206903.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
2400000.0,3.6875,2.6085892588727998E-8,2400000.0,61.199999999999996,18.245222191786585,15.468409586056636,-2.3746701846965723,215994.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
2500000.0,3.84375,2.7206713437206208E-8,2500000.0,58.9,15.393799662398628,15.950920245398764,-0.24390243902439046,225085.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
2600000.0,4.0,2.8321390143699108E-8,2600000.0,62.5,20.409202818575434,16.29464285714285,-4.1666666666666705,234176.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
2700000.0,4.15625,2.9426042197479132E-8,2700000.0,54.50000000000001,10.808797584976668,2.7777777777777803,-22.972972972972965,243267.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
2800000.0,4.3125,3.052455010927385E-8,2800000.0,54.1,9.899652946811347,-1.1013215859030847,-24.390243902439014,252358.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
2900000.0,4.46875,3.162079438981082E-8,2900000.0,63.6,23.27150084317033,19.646799116997787,2.1505376344086042,261449.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
3000000.0,4.625,3.271089452836248E-8,3000000.0,62.7,21.231574946150268,22.453222453222452,0.26737967914438526,270540.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
3100000.0,4.765625,3.3682962465617385E-8,3100000.0,55.7,13.0526943851275,6.736842105263165,-24.438202247191,279631.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
3200000.0,4.921875,3.475689380947086E-8,3200000.0,51.800000000000004,5.0953181669613485,-7.349665924276176,-20.802005012531318,288722.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
3300000.0,5.0625,3.572139475080702E-8,3300000.0,61.3,19.206005895665093,19.374999999999996,-1.3089005235602107,297813.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
3400000.0,5.203125,3.673770051035616E-8,3400000.0,56.2,15.186956605929169,4.782608695652178,-26.95652173913043,306903.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
3500000.0,5.359375,3.786466550081968E-8,3500000.0,59.8,15.307085552484315,9.662921348314592,-9.239130434782616,315994.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
3600000.0,5.515625,3.8985486349297896E-8,3600000.0,60.3,16.78335163959855,19.959677419354836,-3.6553524804177577,325085.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
3700000.0,5.65625,3.9985202925486696E-8,3700000.0,56.599999999999994,14.234672906061272,7.264957264957247,-13.020833333333345,334176.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
3800000.0,5.796875,4.097938977388872E-8,3800000.0,54.50000000000001,11.317798030670401,5.991735537190088,-20.052770448548802,343267.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
3900000.0,5.9375,4.197153935415877E-8,3900000.0,62.5,21.64908820432247,21.383647798742132,4.336734693877554,352358.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
4000000.0,6.09375,4.306778363469574E-8,4000000.0,62.1,19.70338983050847,20.711297071129703,-1.0666666666666678,361449.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
4100000.0,6.25,4.41578837732474E-8,4100000.0,61.199999999999996,17.36769247151528,19.834710743801647,-9.295774647887333,370540.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
4200000.0,6.40625,4.524183976981375E-8,4200000.0,62.5,21.617644599769235,20.212765957446805,2.343750000000002,379631.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
4300000.0,6.546875,4.6211870438936684E-8,4300000.0,62.1,21.41038295331072,23.279352226720647,8.23244552058112,388722.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
4400000.0,6.703125,4.7279657640804847E-8,4400000.0,51.7,4.87631952103356,3.976143141153085,-19.851116625310162,397813.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
4500000.0,6.84375,4.829596340035399E-8,4500000.0,56.39999999999999,13.804010881255138,11.201629327902229,-10.10101010101011,406903.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
4600000.0,7.0,4.941904788008995E-8,4600000.0,53.2,8.190649570771672,3.5051546391752613,-18.78172588832486,415994.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
4700000.0,7.15625,5.053986872856816E-8,4700000.0,62.8,22.324907395420503,21.51898734177215,0.26809651474530855,425085.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
4800000.0,7.296875,5.153958530475696E-8,4800000.0,54.1,9.642111894169068,3.7735849056603805,-16.202531645569607,434176.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
4900000.0,7.453125,5.2648117869264555E-8,4900000.0,60.6,16.765252661821865,13.024282560706391,-11.931818181818194,443267.0,9208.0,0.0,1.0,0.0,?,0.0,0.0
5000000.0,7.609375,5.384008141441478E-8,5000000.0,53.0,9.340966695343226,2.6915113871635636,-20.822622107969142,452358.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
5100000.0,7.765625,5.50259008175797E-8,5100000.0,54.7,9.041258724409062,9.40000000000001,6.211180124223608,461449.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
5200000.0,7.921875,5.6205576078759305E-8,5200000.0,52.2,3.8801840352626966,6.4579256360078325,-0.8438818565400852,470540.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
5300000.0,8.0625,5.726175408603417E-8,5300000.0,53.800000000000004,7.069179151312309,9.23379174852653,1.9108280254777086,479631.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
5400000.0,8.21875,5.8429141063243154E-8,5400000.0,52.6,4.612796374481307,7.05882352941177,-0.2114164904862581,488722.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
5500000.0,8.359375,5.93901495449245E-8,5500000.0,51.4,2.810496466381633,7.074569789674959,2.994011976047907,497813.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
5600000.0,8.515625,6.061283380000128E-8,5600000.0,54.0,7.5291383725932635,12.045889101338442,4.3659043659043695,506903.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
5700000.0,8.671875,6.182937391309275E-8,5700000.0,52.6,5.096745258841627,4.242424242424246,3.658536585365857,515994.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
5800000.0,8.828125,6.303976988419891E-8,5800000.0,55.2,9.895414320193085,7.628865979381451,5.084745762711869,525085.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
5900000.0,8.984375,6.424402171331976E-8,5900000.0,51.6,2.061167252815747,6.019417475728161,-9.255079006772018,534176.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
6000000.0,9.140625,6.544212940045529E-8,6000000.0,51.4,2.6282362086720905,1.6194331983805683,1.0183299389002045,543267.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
6100000.0,9.296875,6.663409294560552E-8,6100000.0,55.7,10.667473280903419,9.406952965235183,1.5555555555555571,552358.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
6200000.0,9.46875,6.793849428908693E-8,6200000.0,52.800000000000004,5.537654852202448,6.903353057199217,-0.42553191489361747,561449.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
6300000.0,9.609375,6.900020202414858E-8,6300000.0,54.50000000000001,8.960671009876275,12.162162162162172,6.570841889117049,570540.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
6400000.0,9.75,7.005638003142344E-8,6400000.0,54.2,8.036560487044813,6.5306122448979655,3.3755274261603407,579631.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
6500000.0,9.90625,7.122376700863244E-8,6500000.0,53.300000000000004,5.889275810924104,4.498977505112479,0.21367521367521386,588722.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
6600000.0,10.0625,7.22915542105006E-8,6600000.0,51.4,2.3876855839064226,5.813953488372098,-2.1008403361344556,597813.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
6700000.0,10.21875,7.351423846557737E-8,6700000.0,53.400000000000006,6.308682732247911,6.613226452905817,3.1185031185031216,606903.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
6800000.0,10.375,7.473077857866885E-8,6800000.0,54.1,7.729049066435093,10.176125244618405,2.547770700636945,615994.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
6900000.0,10.53125,7.594117454977501E-8,6900000.0,52.400000000000006,4.800000000000004,-1.9271948608137062,5.367793240556665,625085.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
7000000.0,10.671875,7.702500119598376E-8,7000000.0,54.50000000000001,8.4551249034252,3.8054968287526463,2.9850746268656745,634176.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
7100000.0,10.828125,7.82231088831193E-8,7100000.0,53.900000000000006,7.1687474828836235,7.984031936127751,2.947368421052634,643267.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
7200000.0,10.984375,7.941507242826953E-8,7200000.0,51.300000000000004,2.342183364081179,-0.828157349896481,0.40899795501022534,652358.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
7300000.0,11.140625,8.060089183143445E-8,7300000.0,53.5,6.449925562306358,2.5157232704402537,-3.104212860310424,661449.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
7400000.0,11.296875,8.178056709261405E-8,7400000.0,53.400000000000006,6.441859437928402,7.539682539682547,0.214132762312634,670540.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
7500000.0,11.453125,8.295409821180835E-8,7500000.0,55.50000000000001,10.814494147827483,14.423076923076936,8.624229979466127,679631.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
7600000.0,11.609375,8.412148518901734E-8,7600000.0,55.60000000000001,10.726852317281594,8.829568788501035,6.526315789473689,688722.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
7700000.0,11.765625,8.51892723908855E-8,7700000.0,51.9,3.589439335508758,-1.0504201680672278,1.0288065843621408,697813.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
7800000.0,11.921875,8.641195664596227E-8,7800000.0,53.0,5.864444800512734,13.284132841328415,4.081632653061228,706903.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
7900000.0,12.0625,8.750684274774459E-8,7900000.0,52.1,3.254994789099932,6.627680311890844,-3.4557235421166337,715994.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
8000000.0,12.203125,8.859619912174013E-8,8000000.0,52.1,4.244430584208932,4.200000000000004,4.960317460317465,725085.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
8100000.0,12.359375,8.980045095086098E-8,8100000.0,55.7,11.411339348563386,11.57684630738524,9.591836734693885,734176.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
8200000.0,12.515625,9.099855863799651E-8,8200000.0,51.800000000000004,2.9478092783505128,2.231237322515215,-5.240174672489088,743267.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
8300000.0,12.671875,9.219052218314674E-8,8300000.0,52.5,4.31643662323638,3.258655804480655,-2.8138528138528165,752358.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
8400000.0,12.828125,9.337634158631166E-8,8400000.0,52.800000000000004,5.258551820762016,-0.21231422505307876,-1.2875536480686707,761449.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
8500000.0,12.984375,9.455601684749127E-8,8500000.0,53.7,6.953376205787796,4.140786749482405,-2.433628318584073,770540.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
8600000.0,13.140625,9.572954796668556E-8,8600000.0,55.1,9.856010536206169,6.846473029045649,6.458333333333339,779631.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
8700000.0,13.296875,9.689693494389455E-8,8700000.0,51.6,3.151575787893955,2.8112449799196813,1.8255578093306306,788722.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
8800000.0,13.453125,9.796472214576271E-8,8800000.0,53.900000000000006,7.740215659747525,6.6801619433198445,7.243460764587531,797813.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
8900000.0,13.609375,9.918740640083949E-8,8900000.0,53.800000000000004,6.743909085403999,5.521472392638041,-2.8953229398663725,806903.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
9000000.0,13.765625,1.0040394651393096E-7,9000000.0,50.9,1.5850617749164229,2.1912350597609582,-0.8213552361396311,815994.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
9100000.0,13.921875,1.0161434248503712E-7,9100000.0,53.900000000000006,6.811455924446539,8.71287128712872,-2.672605790645882,825085.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
9200000.0,14.078125,1.0281859431415797E-7,9200000.0,53.400000000000006,5.855542805424016,8.806262230919772,-0.8658008658008667,834176.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
9300000.0,14.234375,1.040167020012935E-7,9300000.0,55.1,9.905932965198403,4.872881355932208,7.039337474120089,843267.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
9400000.0,14.390625,1.0520866554644373E-7,9400000.0,54.400000000000006,8.360128617363358,12.476007677543196,2.978723404255322,852358.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
9500000.0,14.546875,1.0639448494960865E-7,9500000.0,54.6,8.893517793858557,-0.2207505518763799,3.6093418259023387,861449.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
9600000.0,14.703125,1.0757416021078826E-7,9600000.0,50.8,1.4549416741443282,6.285714285714292,0.0,870540.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
9700000.0,14.859375,1.0874769132998255E-7,9700000.0,51.4,2.7252930250432255,2.6052104208416855,-0.8298755186721999,879631.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
9800000.0,15.015625,1.0991507830719154E-7,9800000.0,50.8,0.9980682549903342,1.402805611222446,-6.956521739130442,888722.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
9900000.0,15.171875,1.109828655090597E-7,9900000.0,50.5,0.6351322054584545,3.3203125000000027,-2.6970954356846497,897813.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
1.0E7,15.328125,1.1220554976413648E-7,1.0E7,53.800000000000004,6.826661288696187,8.514851485148522,2.7368421052631606,906903.0,9112.0,0.0,1.0,0.0,?,0.0,0.0
